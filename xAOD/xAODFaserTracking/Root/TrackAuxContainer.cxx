// Local include(s):
#include "xAODFaserTracking/TrackAuxContainer.h"

namespace xAOD {

   TrackAuxContainer::TrackAuxContainer()
      : AuxContainerBase() {

      AUX_VARIABLE( x0 );
      AUX_VARIABLE( y0 );
      AUX_VARIABLE( phi0 );
      AUX_VARIABLE( theta );
      AUX_VARIABLE( qOverP );

      AUX_VARIABLE( definingParametersCovMatrix );

      AUX_VARIABLE( vx );
      AUX_VARIABLE( vy );
      AUX_VARIABLE( vz );
      
      AUX_VARIABLE( clusterLinks);
      AUX_VARIABLE( hitPattern );
     
      AUX_VARIABLE( chiSquared          );
      AUX_VARIABLE( numberDoF );

      AUX_VARIABLE( trackFitter          );
      AUX_VARIABLE( particleHypothesis );

      // TrackSummary information
#ifndef XAODTRACK_SUMMARYDYNAMIC
      // uint8_ts
      AUX_VARIABLE( numberOfContribStripLayers        );
      AUX_VARIABLE( numberOfStripHits                   );
      AUX_VARIABLE( numberOfStripOutliers               );
      AUX_VARIABLE( numberOfStripHoles                  );
      AUX_VARIABLE( numberOfStripDoubleHoles            );
      AUX_VARIABLE( numberOfStripSharedHits             );
      AUX_VARIABLE( numberOfStripDeadSensors            );
      AUX_VARIABLE( numberOfStripSpoiltHits             );

      AUX_VARIABLE( numberOfOutliersOnTrack           );
      AUX_VARIABLE( standardDeviationOfChi2OS         );
#endif

   }

   void TrackAuxContainer::dump() const {
     std::cout<<" Dumping TrackAuxContainer"<<std::endl;
     std::cout<<"x0:";
     std::copy(x0.begin(), x0.end(),
       std::ostream_iterator<float>(std::cout, ", "));
     std::cout<<"y0:";
     std::copy(y0.begin(), y0.end(),
       std::ostream_iterator<float>(std::cout, ", "));
     std::cout<<"phi0:";
     std::copy(phi0.begin(), phi0.end(),
       std::ostream_iterator<float>(std::cout, ", "));
     std::cout<<"theta:";
     std::copy(theta.begin(), theta.end(),
       std::ostream_iterator<float>(std::cout, ", "));
     std::cout<<"qOverP:";
     std::copy(qOverP.begin(), qOverP.end(),
       std::ostream_iterator<float>(std::cout, ", "));
     std::cout<<"definingParametersCovMatrix: ["<<&definingParametersCovMatrix<<"]";
     for (unsigned int i=0; i<definingParametersCovMatrix.size();++i){
     std::copy(definingParametersCovMatrix[i].begin(), definingParametersCovMatrix[i].end(),
       std::ostream_iterator<float>(std::cout, ", "));
        std::cout<<std::endl;
     }
   }

} // namespace xAOD
