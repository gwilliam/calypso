/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

// $Id: FaserTruthVertex.cxx 624338 2014-10-27 15:08:55Z krasznaa $

// System include(s):
#include <cmath>

// xAOD include(s):
#include "xAODCore/AuxStoreAccessorMacros.h"

// Local include(s):
#include "xAODFaserTruth/FaserTruthVertex.h"
#include "xAODFaserTruth/FaserTruthParticleContainer.h"

namespace xAOD {

   FaserTruthVertex::FaserTruthVertex()
   : SG::AuxElement(), m_v4(), m_v4Cached( false ) {

   }

   /////////////////////////////////////////////////////////////////////////////
   //
   //            Implementation for the "MC specific" functions
   //

   AUXSTORE_PRIMITIVE_SETTER_AND_GETTER( FaserTruthVertex, int, id, setId )
   AUXSTORE_PRIMITIVE_SETTER_AND_GETTER( FaserTruthVertex, int, barcode,
                                         setBarcode )

   //
   /////////////////////////////////////////////////////////////////////////////

   /////////////////////////////////////////////////////////////////////////////
   //
   //           Implementation for the links to the truth particles
   //

   AUXSTORE_OBJECT_SETTER_AND_GETTER( FaserTruthVertex, FaserTruthVertex::TPLinks_t,
                                      incomingParticleLinks,
                                      setIncomingParticleLinks )

   /// Accessor for the incoming particles
   static SG::AuxElement::Accessor< FaserTruthVertex::TPLinks_t >
      incomingParticleLinksAcc( "incomingParticleLinks" );

   size_t FaserTruthVertex::nIncomingParticles() const {
   
      // Check if the variable is available:
      if( ! incomingParticleLinksAcc.isAvailable( *this ) ) {
         // If not, just tell the user that there aren't any incoming particles:
         return 0;
      }

      // Return the size of the vector:
      return incomingParticleLinksAcc( *this ).size();
   }

   const FaserTruthParticle* FaserTruthVertex::incomingParticle( size_t index ) const {

      // Check that the variable exists, and that it has enough elements in it:
      if( ( ! incomingParticleLinksAcc.isAvailable( *this ) ) ||
          ( incomingParticleLinksAcc( *this ).size() <= index ) ) {
         return 0;
      }

      // Retrieve the link object and check its validity:
      const TPLink_t& ipl = incomingParticleLinksAcc( *this )[ index ];
      if( ! ipl.isValid() ) {
         return 0;
      }

      // Finally, de-reference the link:
      return *ipl;
   }

   void FaserTruthVertex::addIncomingParticleLink( const TPLink_t& link ) {

      incomingParticleLinksAcc( *this ).push_back( link );
      return;
   }

   void FaserTruthVertex::clearIncomingParticleLinks() {

      incomingParticleLinksAcc( *this ).clear();
      return;
   }

   AUXSTORE_OBJECT_SETTER_AND_GETTER( FaserTruthVertex, FaserTruthVertex::TPLinks_t,
                                      outgoingParticleLinks,
                                      setOutgoingParticleLinks )

   /// Accessor for the outgoing particles
   static SG::AuxElement::Accessor< FaserTruthVertex::TPLinks_t >
      outgoingParticleLinksAcc( "outgoingParticleLinks" );

   size_t FaserTruthVertex::nOutgoingParticles() const {

      // Check if the variable is available:
      if( ! outgoingParticleLinksAcc.isAvailable( *this ) ) {
         // If not, just tell the user that there aren't any outgoing particles:
         return 0;
      }

      // Return the size of the vector:
      return outgoingParticleLinksAcc( *this ).size();
   }

   const FaserTruthParticle* FaserTruthVertex::outgoingParticle( size_t index ) const {

      // Check that the variable exists, and that it has enough elements in it:
      if( ( ! outgoingParticleLinksAcc.isAvailable( *this ) ) ||
          ( outgoingParticleLinksAcc( *this ).size() <= index ) ) {
         return 0;
      }

      // Retrieve the link object and check its validity:
      const TPLink_t& opl = outgoingParticleLinksAcc( *this )[ index ];
      if( ! opl.isValid() ) {
         return 0;
      }
      
      // Finally, de-reference the link:
      return *opl;
   }

   void FaserTruthVertex::addOutgoingParticleLink( const TPLink_t& link ) {

      outgoingParticleLinksAcc( *this ).push_back( link );
      return;
   }

   void FaserTruthVertex::clearOutgoingParticleLinks() {

      outgoingParticleLinksAcc( *this ).clear();
      return;
   }

   //
   /////////////////////////////////////////////////////////////////////////////

   /////////////////////////////////////////////////////////////////////////////
   //
   //     Implementation of the functions specifying the vertex's position
   //

   AUXSTORE_PRIMITIVE_GETTER( FaserTruthVertex, float, x )

   void FaserTruthVertex::setX( float x ) {

      static SG::AuxElement::Accessor< float > acc( "x" );
      m_v4Cached = false;
      acc( *this ) = x;
      return;
   }

   AUXSTORE_PRIMITIVE_GETTER( FaserTruthVertex, float, y )

   void FaserTruthVertex::setY( float y ) {

      static SG::AuxElement::Accessor< float > acc( "y" );
      m_v4Cached = false;
      acc( *this ) = y;
      return;
   }

   AUXSTORE_PRIMITIVE_GETTER( FaserTruthVertex, float, z )

   void FaserTruthVertex::setZ( float z ) {

      static SG::AuxElement::Accessor< float > acc( "z" );
      m_v4Cached = false;
      acc( *this ) = z;
      return;
   }

   float FaserTruthVertex::perp() const {

      // Do the calculation by hand. Could make it faster than this even in a
      // future iteration...
      return std::sqrt( x() * x() + y() * y() );
   }

   float FaserTruthVertex::eta() const {

      // This is not necessarily what Andy was thinking about...
      return v4().Eta();
   }

   float FaserTruthVertex::phi() const {

      // This is not necessarily what Andy was thinking about...
      return v4().Phi();
   }

   AUXSTORE_PRIMITIVE_GETTER( FaserTruthVertex, float, t )

   void FaserTruthVertex::setT( float t ) {

      static SG::AuxElement::Accessor< float > acc( "t" );
      m_v4Cached = false;
      acc( *this ) = t;
      return;
   }

   const FaserTruthVertex::FourVec_t& FaserTruthVertex::v4() const {

      // Cache the 4-vector if it's not already:
      if( ! m_v4Cached ) {
         m_v4.SetXYZT( x(), y(), z(), t() );
         m_v4Cached = true;
      }

      // Return the cached object:
      return m_v4;
   }

   //
   /////////////////////////////////////////////////////////////////////////////

   FaserType::ObjectType FaserTruthVertex::faserType() const {

      return FaserType::FaserTruthVertex;
   }
   
   Type::ObjectType FaserTruthVertex::type() const {

      return Type::Other;
   }

   void FaserTruthVertex::toPersistent() {

      // Prepare the incoming particle links for persistification:
      if( incomingParticleLinksAcc.isAvailableWritable( *this ) ) {
         TPLinks_t::iterator itr = incomingParticleLinksAcc( *this ).begin();
         TPLinks_t::iterator end = incomingParticleLinksAcc( *this ).end();
         for( ; itr != end; ++itr ) {
            itr->toPersistent();
         }
      }

      // Prepare the outgoing particle links for persistification:
      if( outgoingParticleLinksAcc.isAvailableWritable( *this ) ) {
         TPLinks_t::iterator itr = outgoingParticleLinksAcc( *this ).begin();
         TPLinks_t::iterator end = outgoingParticleLinksAcc( *this ).end();
         for( ; itr != end; ++itr ) {
            itr->toPersistent();
         }
      }

      return;
   }

} // namespace xAOD
