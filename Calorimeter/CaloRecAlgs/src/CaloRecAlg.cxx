#include "CaloRecAlg.h"

CaloRecAlg::CaloRecAlg(const std::string& name, 
		       ISvcLocator* pSvcLocator)
  : AthReentrantAlgorithm(name, pSvcLocator) { 

}

StatusCode 
CaloRecAlg::initialize() {
  ATH_MSG_INFO(name() << "::initalize()" );

  // Initalize tools
  //ATH_CHECK( m_recoTool.retrieve() );

  // Set key to read calo hits from
  ATH_CHECK( m_caloWaveHitContainerKey.initialize() );

  // Set key to read preshower hits from
  ATH_CHECK( m_preshowerWaveHitContainerKey.initialize() );

  // Set key to write container
  ATH_CHECK( m_caloHitContainerKey.initialize() );

  return StatusCode::SUCCESS;
}

StatusCode 
CaloRecAlg::finalize() {
  ATH_MSG_INFO(name() << "::finalize()");

  return StatusCode::SUCCESS;
}

StatusCode 
CaloRecAlg::execute(const EventContext& ctx) const {
  ATH_MSG_DEBUG("Executing");

  ATH_MSG_DEBUG("Run: " << ctx.eventID().run_number() 
		<< " Event: " << ctx.eventID().event_number());

  // Find the input waveform hit containers
  SG::ReadHandle<xAOD::WaveformHitContainer> caloWaveHitHandle(m_caloWaveHitContainerKey, ctx);

  ATH_CHECK( caloWaveHitHandle.isValid() );
  ATH_MSG_DEBUG("Found ReadHandle for WaveformHitContainer " << m_caloWaveHitContainerKey);

  if (caloWaveHitHandle->size() == 0) {
    ATH_MSG_DEBUG("Calorimeter Waveform Hit container found with zero length!");
  }

  SG::ReadHandle<xAOD::WaveformHitContainer> preshowerWaveHitHandle(m_preshowerWaveHitContainerKey, ctx);

  ATH_CHECK( preshowerWaveHitHandle.isValid() );
  ATH_MSG_DEBUG("Found ReadHandle for WaveformHitContainer " << m_preshowerWaveHitContainerKey);

  if (preshowerWaveHitHandle->size() == 0) {
    ATH_MSG_DEBUG("Preshower Waveform Hit container found with zero length!");
  }

  // Find the output waveform container
  SG::WriteHandle<xAOD::CalorimeterHitContainer> caloHitContainerHandle(m_caloHitContainerKey, ctx);
  ATH_CHECK( caloHitContainerHandle.record( std::make_unique<xAOD::CalorimeterHitContainer>(),
					std::make_unique<xAOD::CalorimeterHitAuxContainer>() ) );

  ATH_MSG_DEBUG("WaveformsHitContainer '" << caloHitContainerHandle.name() << "' initialized");

  // Reconstruct all waveforms
  //CHECK( m_recoTool->reconstructAll(*waveformHandle, clockptr, hitContainerHandle.ptr()) );

  // Find peak time (most significant hit)
  const xAOD::WaveformHit* peakHit = findPeakHit(*caloWaveHitHandle);
  if (peakHit == NULL) return StatusCode::SUCCESS;

  // Create a new calo hit
  xAOD::CalorimeterHit* calo_hit = new xAOD::CalorimeterHit();
  caloHitContainerHandle->push_back(calo_hit);

  calo_hit->set_raw_energy(-1.);  // Dummy value

  // Find closest hits in time per channel
  std::map<int, const xAOD::WaveformHit*> hitMap;
  for ( const auto& hit : *caloWaveHitHandle ) {
    int channel = hit->channel();
    if (hitMap.count(channel) == 0)
      hitMap[channel] = hit;
    else {
      if (abs(hitMap[channel]->localtime() - peakHit->localtime()) > 
	  abs(hit->localtime() - peakHit->localtime()))
	hitMap[channel] = hit;
    }
  }

  // For each hit found, insert these into the caloHit
  // Clear before association
  calo_hit->clearCaloWaveformLinks();
  for ( const auto& [chan, hit] : hitMap ) {
    ATH_MSG_VERBOSE("Found hit " << *hit);
    calo_hit->addCaloHit(caloWaveHitHandle.get(), hit);
  }

  ATH_MSG_DEBUG("CaloHitContainer '" << caloHitContainerHandle.name() << "' filled with "<< caloHitContainerHandle->size() <<" items");

  return StatusCode::SUCCESS;
}

const xAOD::WaveformHit* 
CaloRecAlg::findPeakHit(const xAOD::WaveformHitContainer& hitContainer) const {

  const xAOD::WaveformHit* peakHit = NULL;
  for( const auto& hit : hitContainer ) {
    if (peakHit == NULL) {
      peakHit = hit;
    } else {
      if ( hit->peak() > peakHit->peak() ) peakHit = hit;
    }
  }

  // Didn't find anything?
  if (peakHit == NULL) return NULL;
  if (peakHit->status_bit(xAOD::WaveformStatus::THRESHOLD_FAILED)) return NULL;
  return peakHit;
}
