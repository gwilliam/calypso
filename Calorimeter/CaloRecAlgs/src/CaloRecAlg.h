#ifndef CALORECALGS_CALORECALG_H
#define CALORECALGS_CALORECALG_H

// Base class
#include "AthenaBaseComps/AthReentrantAlgorithm.h"

// Data classes
#include "xAODFaserWaveform/WaveformHitContainer.h"

#include "xAODFaserCalorimeter/CalorimeterHit.h"
#include "xAODFaserCalorimeter/CalorimeterHitContainer.h"
#include "xAODFaserCalorimeter/CalorimeterHitAuxContainer.h"

// Tool classes
//#include "CaloRecTools/ICalorimeterReconstructionTool.h"

// Handles
#include "StoreGate/ReadHandleKey.h"
#include "StoreGate/WriteHandleKey.h"

// Gaudi
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ToolHandle.h"

// STL
#include <string>

class CaloRecAlg : public AthReentrantAlgorithm {

 public:
  // Constructor
  CaloRecAlg(const std::string& name, ISvcLocator* pSvcLocator);
  virtual ~CaloRecAlg() = default;

  /** @name Usual algorithm methods */
  //@{
  virtual StatusCode initialize() override;
  virtual StatusCode execute(const EventContext& ctx) const override;
  virtual StatusCode finalize() override;
  //@}

 private:

  /** @name Disallow default instantiation, copy, assignment */
  //@{
  CaloRecAlg() = delete;
  CaloRecAlg(const CaloRecAlg&) = delete;
  CaloRecAlg &operator=(const CaloRecAlg&) = delete;
  //@}

  /**
   * @name Reconstruction tool
   */
  //ToolHandle<ICalorimeterReconstructionTool> m_recoTool
  //{this, "CalorimeterReconstructionTool", "CalorimeterReconstructionTool"};

  /**
   * @name Input raw waveform data using SG::ReadHandleKey
   */
  //@{
  SG::ReadHandleKey<xAOD::WaveformHitContainer> m_caloWaveHitContainerKey
    {this, "CaloWaveHitContainerKey", ""};
  //@}

  //@{
  SG::ReadHandleKey<xAOD::WaveformHitContainer> m_preshowerWaveHitContainerKey
    {this, "PreshowerWaveHitContainerKey", ""};
  //@}

  /**
   * @name Output data using SG::WriteHandleKey
   */
  //@{
  SG::WriteHandleKey<xAOD::CalorimeterHitContainer> m_caloHitContainerKey
    {this, "CaloHitContainerKey", ""};
  //@}

  const xAOD::WaveformHit* 
    findPeakHit(const xAOD::WaveformHitContainer& hitContainer) const;

};

#endif // CALORECALGS_CALORECALG_H
