#include "FaserSpacePoints.h"
#include "Identifier/Identifier.h"
#include "TrackerSpacePoint/FaserSCT_SpacePoint.h"
#include "TrackerPrepRawData/FaserSCT_ClusterCollection.h"

namespace Tracker {
  FaserSpacePoints::FaserSpacePoints(const std::string& name, ISvcLocator* pSvcLocator)
    : AthReentrantAlgorithm(name, pSvcLocator) { }

  
  StatusCode FaserSpacePoints::initialize() {
    ATH_MSG_INFO(name() << "::" << __FUNCTION__);

    if ( m_sct_spcontainer.key().empty()) {
      ATH_MSG_FATAL( "SCTs selected and no name set for SCT clusters");
      return StatusCode::FAILURE;
    }
    ATH_CHECK( m_sct_spcontainer.initialize() );

    ATH_CHECK(detStore()->retrieve(m_idHelper, "FaserSCT_ID"));

    std::string filePath = m_filePath;
    m_outputFile = TFile::Open(filePath.c_str(), "RECREATE");
    if (m_outputFile == nullptr) {
      ATH_MSG_ERROR("Unable to open output file at " << m_filePath);
      return StatusCode::FAILURE;
    }
    m_outputFile->cd();

    std::string treeName = m_treeName;
    m_outputTree = new TTree(treeName.c_str(), "tree");
    if (m_outputTree == nullptr) {
      ATH_MSG_ERROR("Unable to create TTree");
      return StatusCode::FAILURE;
    }

    FaserSpacePoints::initializeTree();
    return StatusCode::SUCCESS;
  }

  
  StatusCode FaserSpacePoints::finalize() {
    ATH_MSG_INFO(name() << "::" << __FUNCTION__);
    m_outputFile->cd();
    m_outputTree->Write();
    return StatusCode::SUCCESS;
  }

  
  StatusCode FaserSpacePoints::execute(const EventContext& ctx) const {
    ATH_MSG_INFO(name() << "::" << __FUNCTION__);
  
    m_run = ctx.eventID().run_number();
    m_event = ctx.eventID().event_number();
    ATH_MSG_DEBUG("run = " << m_run);
    ATH_MSG_DEBUG("event = " << m_event);

    SG::ReadHandle<FaserSCT_SpacePointContainer> sct_spcontainer( m_sct_spcontainer, ctx );
    if (!sct_spcontainer.isValid()){
      ATH_MSG_FATAL("Could not find the data object " << sct_spcontainer.name());
      return StatusCode::RECOVERABLE;
    }

    m_nCollections = sct_spcontainer->size();
    ATH_MSG_DEBUG("nCollections = " << m_nCollections);
    FaserSCT_SpacePointContainer::const_iterator it = sct_spcontainer->begin();
    FaserSCT_SpacePointContainer::const_iterator it_end = sct_spcontainer->end();

    for(; it != it_end; ++it) {
      const FaserSCT_SpacePointCollection* col = *it;
      m_nSpacepoints = col->size();
      ATH_MSG_DEBUG("nSpacepoints = " << m_nSpacepoints);

      FaserSCT_SpacePointCollection::const_iterator it_sp = col->begin();
      FaserSCT_SpacePointCollection::const_iterator it_sp_end = col->end();
      for (; it_sp != it_sp_end; ++it_sp) {
        const FaserSCT_SpacePoint* sp = *it_sp;
        const auto id = sp->clusterList().first->identify();
        int station = m_idHelper->station(id);
        int plane = m_idHelper->layer(id);
        m_layer = (station - 1) * 3 + plane;

        Amg::Vector3D position = sp->globalPosition();
        m_x = position.x();
        m_y = position.y();
        m_z = position.z();

        m_outputTree->Fill();
      }
    }
    return StatusCode::SUCCESS;
  }

  void FaserSpacePoints::initializeTree() {
    m_outputTree->Branch("run", &m_run);
    m_outputTree->Branch("event", &m_event);
    m_outputTree->Branch("layer", &m_layer);
    m_outputTree->Branch("n_collections", &m_nCollections);
    m_outputTree->Branch("n_spacepoints", &m_nSpacepoints);
    m_outputTree->Branch("x", &m_x);
    m_outputTree->Branch("y", &m_y);
    m_outputTree->Branch("z", &m_z);
  }
}
