################################################################################
# Package: TrkDetStripClusterImporter
################################################################################


# Declare the package name:
atlas_subdir( TrkDetStripClusterImporter )

# External dependencies:
find_package( Eigen )
find_package( ROOT COMPONENTS Core Tree MathCore Hist RIO pthread EG )
find_package( XercesC )
find_package( Boost )

# Component(s) in the package:
atlas_add_component( TrkDetStripClusterImporter
                     src/*.cxx
                     src/components/*.cxx
                     INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} ${XERCESC_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS}  ${EIGEN_INCLUDE_DIRS}
                     LINK_LIBRARIES ${ROOT_LIBRARIES} ${XERCESC_LIBRARIES} ${Boost_LIBRARIES} ${EIGEN_LIBRARIES} 
                     AthenaBaseComps xAODFaserTracking GaudiKernel AthenaKernel 
)
# Install files from the package:
atlas_install_headers( TrkDetStripClusterImporter )