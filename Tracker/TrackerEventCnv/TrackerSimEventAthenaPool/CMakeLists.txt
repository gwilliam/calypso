################################################################################
# Package: TrackerSimEventAthenaPool
################################################################################

# Declare the package name:
atlas_subdir( TrackerSimEventAthenaPool )

# External dependencies:
find_package( ROOT COMPONENTS Core Tree MathCore Hist RIO pthread )

# Component(s) in the package:
atlas_add_poolcnv_library( TrackerSimEventAthenaPoolPoolCnv
                           src/*.cxx
                           FILES TrackerSimEvent/FaserSiHitCollection.h 
                           INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                           LINK_LIBRARIES ${ROOT_LIBRARIES} AthenaPoolCnvSvcLib AthenaPoolUtilities AtlasSealCLHEP GaudiKernel TrackerSimEventTPCnv TrackerSimEvent )

atlas_add_dictionary( TrackerSimEventAthenaPoolCnvDict
                      TrackerSimEventAthenaPool/TrackerSimEventAthenaPoolCnvDict.h
                      TrackerSimEventAthenaPool/selection.xml
                      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                      LINK_LIBRARIES ${ROOT_LIBRARIES} AthenaPoolCnvSvcLib AthenaPoolUtilities AtlasSealCLHEP GaudiKernel TrackerSimEventTPCnv TrackerSimEvent )

# Install files from the package:
atlas_install_headers( TrackerSimEventAthenaPool )
#atlas_install_joboptions( share/*.py )

