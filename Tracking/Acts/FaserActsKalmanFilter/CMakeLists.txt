# Declare the package name:
atlas_subdir(FaserActsKalmanFilter)

# External dependencies:
find_package( CLHEP )
find_package( Eigen )
find_package( Boost )
find_package( Acts COMPONENTS Core )

# Component(s) in the package:
atlas_add_library( FaserActsKalmanFilterLib
    PUBLIC_HEADERS FaserActsKalmanFilter
    INCLUDE_DIRS ${CLHEP_INCLUDE_DIRS} ${EIGEN_INCLUDE_DIRS} ${BOOST_INCLUDE_DIRS}
    LINK_LIBRARIES ${CLHEP_LIBRARIES} ${EIGEN_LIBRARIES}
    AthenaKernel
    ActsCore
    TrackerIdentifier
    TrackerReadoutGeometry
    ActsInteropLib
    FaserActsGeometryLib
    FaserActsGeometryInterfacesLib
)

atlas_add_component(FaserActsKalmanFilter
    FaserActsKalmanFilter/CombinatorialKalmanFilterAlg.h
    FaserActsKalmanFilter/EffPlotTool.h
    FaserActsKalmanFilter/FASERSourceLink.h
    FaserActsKalmanFilter/FaserActsGeometryContainers.h
    FaserActsKalmanFilter/FaserActsKalmanFilterAlg.h
    FaserActsKalmanFilter/FaserActsRecMultiTrajectory.h
    FaserActsKalmanFilter/IdentifierLink.h
    FaserActsKalmanFilter/IndexSourceLink.h
    FaserActsKalmanFilter/ITrackFinderTool.h
    FaserActsKalmanFilter/ITrackSeedTool.h
#    FaserActsKalmanFilter/ClusterTrackSeedTool.h
#    FaserActsKalmanFilter/TruthTrackFinderTool.h
    FaserActsKalmanFilter/Measurement.h
#    FaserActsKalmanFilter/MultiTrackFinderTool.h
    FaserActsKalmanFilter/MyAmbiguitySolver.h
    FaserActsKalmanFilter/PerformanceWriterTool.h
    FaserActsKalmanFilter/PlotHelpers.h
    FaserActsKalmanFilter/ResPlotTool.h
    FaserActsKalmanFilter/RootTrajectoryStatesWriterTool.h
    FaserActsKalmanFilter/RootTrajectorySummaryWriterTool.h
#    FaserActsKalmanFilter/SegmentFitClusterTrackFinderTool.h
#    FaserActsKalmanFilter/SegmentFitTrackFinderTool.h
    FaserActsKalmanFilter/SimWriterTool.h
    FaserActsKalmanFilter/SPSeedBasedInitialParameterTool.h
    FaserActsKalmanFilter/SPSimpleInitialParameterTool.h
    FaserActsKalmanFilter/SummaryPlotTool.h
    FaserActsKalmanFilter/TrackClassification.h
    FaserActsKalmanFilter/TrackSelection.h
    FaserActsKalmanFilter/TrajectoryWriterTool.h
#    FaserActsKalmanFilter/ProtoTrackWriterTool.h
    FaserActsKalmanFilter/TruthBasedInitialParameterTool.h
#    FaserActsKalmanFilter/TruthSeededTrackFinderTool.h
    FaserActsKalmanFilter/ThreeStationTrackSeedTool.h
#    src/ClusterTrackSeedTool.cxx
    src/CombinatorialKalmanFilterAlg.cxx
    src/EffPlotTool.cxx
    src/FaserActsKalmanFilterAlg.cxx
#    src/MultiTrackFinderTool.cxx
    src/PerformanceWriterTool.cxx
    src/PlotHelpers.cxx
    src/ResPlotTool.cxx
    src/RootTrajectoryStatesWriterTool.cxx
    src/RootTrajectorySummaryWriterTool.cxx
#    src/SegmentFitClusterTrackFinderTool.cxx
#    src/SegmentFitTrackFinderTool.cxx
    src/SimWriterTool.cxx
    src/SPSeedBasedInitialParameterTool.cxx
    src/SPSimpleInitialParameterTool.cxx
#    src/ProtoTrackWriterTool.cxx
    src/TrackFindingAlgorithmFunction.cxx
    src/TrackFittingFunction.cxx
    src/TrajectoryWriterTool.cxx
    src/TruthBasedInitialParameterTool.cxx
    src/SummaryPlotTool.cxx
    src/TrackClassification.cxx
    src/TrackSelection.cxx
#    src/TruthTrackFinderTool.cxx
#    src/TruthSeededTrackFinderTool.cxx
    src/ThreeStationTrackSeedTool.cxx
    src/components/FaserActsKalmanFilter_entries.cxx
    PUBLIC_HEADERS FaserActsKalmanFilter
    INCLUDE_DIRS ${CLHEP_INCLUDE_DIRS} ${EIGEN_INCLUDE_DIRS} ${BOOST_INCLUDE_DIRS}
    LINK_LIBRARIES ${CLHEP_LIBRARIES} ${EIGEN_LIBRARIES}
    ActsCore
    EventInfo
    FaserActsKalmanFilterLib
    ActsInteropLib
    FaserActsGeometryLib
    FaserActsGeometryInterfacesLib
    MagFieldInterfaces
    MagFieldElements
    MagFieldConditions
    TrkPrepRawData
    TrkTrack
    TrackerPrepRawData
    TrackerRIO_OnTrack
    TrkRIO_OnTrack
    TrackerSpacePoint
    GeoModelFaserUtilities
    GeneratorObjects
    TrackerIdentifier
    TrackerReadoutGeometry
    Identifier
    TrackerSimEvent
    TrackerSimData
    TrackerSeedFinderLib
)

# Install files from the package:
atlas_install_headers(FaserActsKalmanFilter)
atlas_install_python_modules( python/*.py )
atlas_install_scripts( test/*.py )
