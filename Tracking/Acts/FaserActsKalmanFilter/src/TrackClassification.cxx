#include "TrackerPrepRawData/FaserSCT_Cluster.h"
#include "FaserActsKalmanFilter/TrackClassification.h"

namespace {

/// Increase the hit count for the given particle id by one.
inline void increaseHitCount(std::vector<ParticleHitCount> &particleHitCounts, int particleId) {
  // linear search since there is no ordering
  auto it = std::find_if(particleHitCounts.begin(), particleHitCounts.end(),
                         [=](const ParticleHitCount &phc) {
                           return (phc.particleId == particleId);
                         });
  // either increase count if we saw the particle before or add it
  if (it != particleHitCounts.end()) {
    it->hitCount += 1u;
  } else {
    particleHitCounts.push_back({particleId, 1u});
  }
}

/// Sort hit counts by decreasing values, i.e. majority particle comes first.
inline void sortHitCount(std::vector<ParticleHitCount> &particleHitCounts) {
  std::sort(particleHitCounts.begin(), particleHitCounts.end(),
            [](const ParticleHitCount &lhs, const ParticleHitCount &rhs) {
              return (lhs.hitCount > rhs.hitCount);
            });
}

}  // namespace


/// Identify all particles that contribute to a trajectory.
void identifyContributingParticles(
    const TrackerSimDataCollection& simDataCollection,
    const FaserActsRecMultiTrajectory& trajectories, size_t tip,
    std::vector<ParticleHitCount>& particleHitCounts) {
  particleHitCounts.clear();

  if (not trajectories.hasTrajectory(tip)) {
    return;
  }

  trajectories.multiTrajectory().visitBackwards(tip, [&](const auto& state) {
    // no truth info with non-measurement state
    if (not state.typeFlags().test(Acts::TrackStateFlag::MeasurementFlag)) {
      return true;
    }
    // register all particles that generated this hit
    Identifier id = state.uncalibrated().hit()->identify();
    if (simDataCollection.count(id) == 0) {
      return true;
    }
    const auto& deposits = simDataCollection.find(id)->second.getdeposits();
    int barcode = 0;
    float highestDep = 0;
    for (const TrackerSimData::Deposit &deposit : deposits) {
      if (deposit.second > highestDep) {
        highestDep = deposit.second;
        barcode = deposit.first->barcode();
      }
    }
    increaseHitCount(particleHitCounts, barcode);
    return true;
  });
  sortHitCount(particleHitCounts);
}
