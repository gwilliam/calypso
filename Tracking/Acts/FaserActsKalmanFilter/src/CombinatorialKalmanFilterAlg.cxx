#include "FaserActsKalmanFilter/CombinatorialKalmanFilterAlg.h"

#include "StoreGate/ReadHandle.h"
#include "StoreGate/ReadCondHandleKey.h"
#include "TrackerSpacePoint/FaserSCT_SpacePointCollection.h"
#include "TrackerSpacePoint/FaserSCT_SpacePoint.h"
#include "TrackerIdentifier/FaserSCT_ID.h"
#include "TrkPrepRawData/PrepRawData.h"
#include "TrackerPrepRawData/FaserSCT_Cluster.h"
#include "TrackerRIO_OnTrack/FaserSCT_ClusterOnTrack.h"
#include "TrkRIO_OnTrack/RIO_OnTrack.h"
#include "TrkSurfaces/Surface.h"
#include "Identifier/Identifier.h"
#include "Acts/Geometry/GeometryIdentifier.hpp"
#include "Acts/EventData/TrackParameters.hpp"
#include "FaserActsKalmanFilter/IndexSourceLink.h"
#include "FaserActsKalmanFilter/Measurement.h"
#include "FaserActsKalmanFilter/FaserActsRecMultiTrajectory.h"
#include "Acts/Surfaces/PerigeeSurface.hpp"
#include "Acts/MagneticField/MagneticFieldContext.hpp"
#include "FaserActsKalmanFilter/TrackSelection.h"
#include "FaserActsKalmanFilter/MyAmbiguitySolver.h"
#include <algorithm>

using TrajectoriesContainer = std::vector<FaserActsRecMultiTrajectory>;
std::array<Acts::BoundIndices, 2> indices = {Acts::eBoundLoc0, Acts::eBoundLoc1};


CombinatorialKalmanFilterAlg::CombinatorialKalmanFilterAlg(
    const std::string& name, ISvcLocator* pSvcLocator)
    : AthAlgorithm(name, pSvcLocator) {}


StatusCode CombinatorialKalmanFilterAlg::initialize() {
  ATH_CHECK(m_fieldCondObjInputKey.initialize());
  ATH_CHECK(m_trackingGeometryTool.retrieve());
  ATH_CHECK(m_trackSeedTool.retrieve());
  //  ATH_CHECK(m_trackCollection.initialize());
  if (m_performanceWriter) {
    ATH_CHECK(m_performanceWriterTool.retrieve());
  }
  if (m_statesWriter) {
    ATH_CHECK(m_trajectoryStatesWriterTool.retrieve());
  }
  if (m_summaryWriter) {
    ATH_CHECK(m_trajectorySummaryWriterTool.retrieve());
  }
  ATH_CHECK(detStore()->retrieve(m_idHelper,"FaserSCT_ID"));
  m_fit = makeTrackFinderFunction(m_trackingGeometryTool->trackingGeometry(),
                                  m_resolvePassive, m_resolveMaterial, m_resolveSensitive);
  // FIXME fix Acts logging level
  if (m_actsLogging == "VERBOSE") {
    m_logger = Acts::getDefaultLogger("KalmanFitter", Acts::Logging::VERBOSE);
  } else if (m_actsLogging == "DEBUG") {
    m_logger = Acts::getDefaultLogger("KalmanFitter", Acts::Logging::DEBUG);
  } else {
    m_logger = Acts::getDefaultLogger("KalmanFitter", Acts::Logging::INFO);
  }
  return StatusCode::SUCCESS;
}


StatusCode CombinatorialKalmanFilterAlg::execute() {

  const EventContext& ctx = Gaudi::Hive::currentContext();

  ATH_CHECK(m_trackCollection.initialize());
  SG::WriteHandle<TrackCollection> trackContainer{m_trackCollection,ctx};
  std::unique_ptr<TrackCollection> outputTracks = std::make_unique<TrackCollection>();

  std::shared_ptr<const Acts::TrackingGeometry> trackingGeometry
      = m_trackingGeometryTool->trackingGeometry();

  const FaserActsGeometryContext& gctx = m_trackingGeometryTool->getNominalGeometryContext();
  auto geoctx = gctx.context();
  Acts::MagneticFieldContext magctx = getMagneticFieldContext(ctx);
  Acts::CalibrationContext calctx;

  CHECK(m_trackSeedTool->run());
  std::shared_ptr<const Acts::Surface> initialSurface =
      m_trackSeedTool->initialSurface();
  std::shared_ptr<std::vector<Acts::CurvilinearTrackParameters>> initialParameters =
      m_trackSeedTool->initialTrackParameters();
  std::shared_ptr<std::vector<IndexSourceLink>> sourceLinks =
      m_trackSeedTool->sourceLinks();
  std::shared_ptr<IdentifierLink> idLinks = m_trackSeedTool->idLinks();
  std::shared_ptr<std::vector<Measurement>> measurements = m_trackSeedTool->measurements();
  std::shared_ptr<std::vector<const Tracker::FaserSCT_Cluster*>> clusters = m_trackSeedTool->clusters();

  TrajectoriesContainer trajectories;
  trajectories.reserve(initialParameters->size());

  Acts::PropagatorPlainOptions pOptions;
  pOptions.maxSteps = m_maxSteps;

  Acts::GeometryContext geoContext = m_trackingGeometryTool->getNominalGeometryContext().context();
  Acts::MagneticFieldContext magFieldContext = getMagneticFieldContext(ctx);
  Acts::CalibrationContext calibContext;
  Acts::MeasurementSelector::Config measurementSelectorCfg = {
    {Acts::GeometryIdentifier(), {m_chi2Max, m_nMax}},
  };

  // Set the CombinatorialKalmanFilter options
  CombinatorialKalmanFilterAlg::TrackFinderOptions options(
      geoContext, magFieldContext, calibContext,
      IndexSourceLinkAccessor(), MeasurementCalibrator(*measurements),
      Acts::MeasurementSelector(measurementSelectorCfg),
      Acts::LoggerWrapper{*m_logger}, pOptions, &(*initialSurface));

  // Perform the track finding for all initial parameters
  ATH_MSG_DEBUG("Invoke track finding with " << initialParameters->size() << " seeds.");
  IndexSourceLinkContainer tmp;
  for (const auto& sl : *sourceLinks) {
    tmp.emplace_hint(tmp.end(), sl);
  }
  auto results = (*m_fit)(tmp, *initialParameters, options);

  std::vector<TrackQuality> trackQuality;
  selectTracks(results, trackQuality);
  for (const auto& track : trackQuality) {
    ATH_MSG_VERBOSE("?? " << track.nMeasurements << ", " << track.chi2);
  }

  TrackFinderResult selectedResults;
  for (size_t i = 0; i < std::min(trackQuality.size(), (size_t)m_nTrajectories); ++i) {
    selectedResults.push_back(Acts::Result<Acts::CombinatorialKalmanFilterResult<IndexSourceLink>>(trackQuality[i].track));
  }
  /*
  TrackFinderResult minTrackRequirements {};
  for (auto& result : results) {
    if (not result.ok()) {
      continue;
    }
    auto& ckfResult = result.value();
    auto traj = FaserActsRecMultiTrajectory(ckfResult.fittedStates, ckfResult.lastMeasurementIndices, ckfResult.fittedParameters);
    const auto& mj = traj.multiTrajectory();
    const auto& trackTips = traj.tips();
    size_t maxMeasurements = 0;
    for (const auto& trackTip : trackTips) {
      auto trajState = Acts::MultiTrajectoryHelpers::trajectoryState(mj, trackTip);
      size_t nMeasurements = trajState.nMeasurements;
      if (nMeasurements > maxMeasurements) {
        maxMeasurements = nMeasurements;
      }
    }
    if (maxMeasurements >= m_minNumberMeasurements) {
      minTrackRequirements.push_back(Acts::Result<Acts::CombinatorialKalmanFilterResult<IndexSourceLink>>(ckfResult));
    }
  }

  TrackFinderResult selectedResults {};
  if (minTrackRequirements.size() > 2) {
    std::pair<std::size_t, std::size_t> trackIndices = solveAmbiguity(results);
    selectedResults.push_back(
        Acts::Result<Acts::CombinatorialKalmanFilterResult<IndexSourceLink>>(results.at(trackIndices.first).value()));
    selectedResults.push_back(
        Acts::Result<Acts::CombinatorialKalmanFilterResult<IndexSourceLink>>(results.at(trackIndices.second).value()));
  } else {
    for (auto& result : minTrackRequirements) {
      selectedResults.push_back(
          Acts::Result<Acts::CombinatorialKalmanFilterResult<IndexSourceLink>>(result.value()));
    }
  }
   */
  computeSharedHits(sourceLinks.get(), selectedResults);

  // Loop over the track finding results for all initial parameters
  for (std::size_t iseed = 0; iseed < selectedResults.size(); ++iseed) {
    // The result for this seed
    auto& result = selectedResults[iseed];
    if (result.ok()) {
      // Get the track finding output object
      const auto& trackFindingOutput = result.value();

      std::unique_ptr<Trk::Track> track = makeTrack(geoctx, result);
      if (track) {
        outputTracks->push_back(std::move(track));
      }

      if (!trackFindingOutput.fittedParameters.empty()) {
        const std::unordered_map<size_t, Acts::BoundTrackParameters>& params = trackFindingOutput.fittedParameters;
        for (const auto& surface_params : params) {
          ATH_MSG_VERBOSE("Fitted parameters for track " << iseed << ", surface " << surface_params.first);
          ATH_MSG_VERBOSE("  position: " << surface_params.second.position(geoctx).transpose());
          ATH_MSG_VERBOSE("  momentum: " << surface_params.second.momentum().transpose());
//          const auto& [currentTip, tipState] = trackFindingOutput.activeTips.back();
//          ATH_MSG_VERBOSE("  #measurements: " << tipState.nMeasurements);
        }
      } else {
        ATH_MSG_DEBUG("No fitted parameters for track " << iseed);
      }
      // Create a Trajectories result struct
      trajectories.emplace_back(trackFindingOutput.fittedStates,
                                trackFindingOutput.lastMeasurementIndices,
                                trackFindingOutput.fittedParameters);
    } else {
      ATH_MSG_WARNING("Track finding failed for seed " << iseed << " with error" << result.error());
      // Track finding failed. Add an empty result so the output container has
      // the same number of entries as the input.
      trajectories.push_back(FaserActsRecMultiTrajectory());
    }
  }

  if (m_statesWriter) {
    ATH_CHECK(m_trajectoryStatesWriterTool->write(geoctx, trajectories));
  }
  if (m_summaryWriter) {
    ATH_CHECK(m_trajectorySummaryWriterTool->write(geoctx, trajectories));
  }
  if  (m_performanceWriter) {
    ATH_CHECK(m_performanceWriterTool->write(geoctx, trajectories));
  }
  ATH_CHECK(trackContainer.record(std::move(outputTracks)));

  return StatusCode::SUCCESS;
}


StatusCode CombinatorialKalmanFilterAlg::finalize() {
  return StatusCode::SUCCESS;
}


Acts::MagneticFieldContext CombinatorialKalmanFilterAlg::getMagneticFieldContext(const EventContext& ctx) const {
  SG::ReadCondHandle<FaserFieldCacheCondObj> readHandle{m_fieldCondObjInputKey, ctx};
  if (!readHandle.isValid()) {
    std::stringstream msg;
    msg << "Failed to retrieve magnetic field condition data " << m_fieldCondObjInputKey.key() << ".";
    throw std::runtime_error(msg.str());
  }
  const FaserFieldCacheCondObj* fieldCondObj{*readHandle};
  return Acts::MagneticFieldContext(fieldCondObj);
}

std::unique_ptr<Trk::Track>
CombinatorialKalmanFilterAlg::makeTrack(Acts::GeometryContext& geoCtx, TrackFitterResult& fitResult) const {
  using ConstTrackStateProxy =
  Acts::detail_lt::TrackStateProxy<IndexSourceLink, 6, true>;
  std::unique_ptr<Trk::Track> newtrack = nullptr;
  //Get the fit output object
  const auto& fitOutput = fitResult.value();
  if (fitOutput.fittedParameters.size() > 0) {
    DataVector<const Trk::TrackStateOnSurface>* finalTrajectory = new DataVector<const Trk::TrackStateOnSurface>{};
    std::vector<std::unique_ptr<const Acts::BoundTrackParameters>> actsSmoothedParam;
    // Loop over all the output state to create track state
    fitOutput.fittedStates.visitBackwards(fitOutput.lastMeasurementIndices.front(), [&](const ConstTrackStateProxy& state) {
      auto flag = state.typeFlags();
      if (state.referenceSurface().associatedDetectorElement() != nullptr) {
        // We need to determine the type of state
        std::bitset<Trk::TrackStateOnSurface::NumberOfTrackStateOnSurfaceTypes> typePattern;
        const Trk::TrackParameters *parm;

        // State is a hole (no associated measurement), use predicted para meters
        if (flag[Acts::TrackStateFlag::HoleFlag] == true) {
          const Acts::BoundTrackParameters actsParam(state.referenceSurface().getSharedPtr(),
                                                     state.predicted(),
                                                     state.predictedCovariance());
          parm = ConvertActsTrackParameterToATLAS(actsParam, geoCtx);
          // auto boundaryCheck = m_boundaryCheckTool->boundaryCheck(*p arm);
          typePattern.set(Trk::TrackStateOnSurface::Hole);
        }
          // The state was tagged as an outlier, use filtered parameters
        else if (flag[Acts::TrackStateFlag::OutlierFlag] == true) {
          const Acts::BoundTrackParameters actsParam(state.referenceSurface().getSharedPtr(),
                                                     state.filtered(), state.filteredCovariance());
          parm = ConvertActsTrackParameterToATLAS(actsParam, geoCtx);
          typePattern.set(Trk::TrackStateOnSurface::Outlier);
        }
          // The state is a measurement state, use smoothed parameters
        else {
          const Acts::BoundTrackParameters actsParam(state.referenceSurface().getSharedPtr(),
                                                     state.smoothed(), state.smoothedCovariance());
          actsSmoothedParam.push_back(std::make_unique<const Acts::BoundTrackParameters>(Acts::BoundTrackParameters(actsParam)));
          //  const auto& psurface=actsParam.referenceSurface();
          Acts::Vector2 local(actsParam.parameters()[Acts::eBoundLoc0], actsParam.parameters()[Acts::eBoundLoc1]);
          //  const Acts::Vector3 dir = Acts::makeDirectionUnitFromPhiTheta(actsParam.parameters()[Acts::eBoundPhi], actsParam.parameters()[Acts::eBoundTheta]);
          //  auto pos=actsParam.position(tgContext);
          parm = ConvertActsTrackParameterToATLAS(actsParam, geoCtx);
          typePattern.set(Trk::TrackStateOnSurface::Measurement);
        }
        Tracker::FaserSCT_ClusterOnTrack* measState = nullptr;
        if (state.hasUncalibrated()) {
          const Tracker::FaserSCT_Cluster* fitCluster = state.uncalibrated().hit();
          if (fitCluster->detectorElement() != nullptr) {
            measState = new Tracker::FaserSCT_ClusterOnTrack{
                fitCluster,
                Trk::LocalParameters{
                    Trk::DefinedParameter{fitCluster->localPosition()[0], Trk::loc1},
                    Trk::DefinedParameter{fitCluster->localPosition()[1], Trk::loc2}
                },
                fitCluster->localCovariance(),
                m_idHelper->wafer_hash(fitCluster->detectorElement()->identify())
            };
          }
        }
        double nDoF = state.calibratedSize();
        const Trk::FitQualityOnSurface *quality = new Trk::FitQualityOnSurface(state.chi2(), nDoF);
        const Trk::TrackStateOnSurface *perState = new Trk::TrackStateOnSurface(measState, parm, quality, nullptr, typePattern);
        // If a state was succesfully created add it to the trajectory
        if (perState) {
          finalTrajectory->insert(finalTrajectory->begin(), perState);
        }
      }
      return;
    });

    // Create the track using the states
    const Trk::TrackInfo newInfo(Trk::TrackInfo::TrackFitter::KalmanFitter, Trk::ParticleHypothesis::muon);
    // Trk::FitQuality* q = nullptr;
    // newInfo.setTrackFitter(Trk::TrackInfo::TrackFitter::KalmanFitter     ); //Mark the fitter as KalmanFitter
    newtrack = std::make_unique<Trk::Track>(newInfo, std::move(*finalTrajectory), nullptr);
  }
  return newtrack;
}

const Trk::TrackParameters*
CombinatorialKalmanFilterAlg::ConvertActsTrackParameterToATLAS(const Acts::BoundTrackParameters &actsParameter, const Acts::GeometryContext& gctx) const      {
  using namespace Acts::UnitLiterals;
  std::optional<AmgSymMatrix(5)> cov = std::nullopt;
  if (actsParameter.covariance()){
    AmgSymMatrix(5) newcov(actsParameter.covariance()->topLeftCorner(5, 5));
    // Convert the covariance matrix to GeV
    for(int i=0; i < newcov.rows(); i++){
      newcov(i, 4) = newcov(i, 4)*1_MeV;
    }
    for(int i=0; i < newcov.cols(); i++){
      newcov(4, i) = newcov(4, i)*1_MeV;
    }
    cov =  std::optional<AmgSymMatrix(5)>(newcov);
  }
  const Amg::Vector3D& pos=actsParameter.position(gctx);
  double tphi=actsParameter.get<Acts::eBoundPhi>();
  double ttheta=actsParameter.get<Acts::eBoundTheta>();
  double tqOverP=actsParameter.get<Acts::eBoundQOverP>()*1_MeV;
  double p = std::abs(1. / tqOverP);
  Amg::Vector3D tmom(p * std::cos(tphi) * std::sin(ttheta), p * std::sin(tphi) * std::sin(ttheta), p * std::cos(ttheta));
  const Trk::CurvilinearParameters * curv = new Trk::CurvilinearParameters(pos,tmom,tqOverP>0, cov);
  return curv;
}

void CombinatorialKalmanFilterAlg::computeSharedHits(std::vector<IndexSourceLink>* sourceLinks, TrackFinderResult& results) const {
  // Compute shared hits from all the reconstructed tracks
  // Compute nSharedhits and Update ckf results
  // hit index -> list of multi traj indexes [traj, meas]
  static_assert(Acts::SourceLinkConcept<IndexSourceLink>,
                "Source link does not fulfill SourceLinkConcept");

  std::vector<std::size_t> firstTrackOnTheHit(
      sourceLinks->size(), std::numeric_limits<std::size_t>::max());
  std::vector<std::size_t> firstStateOnTheHit(
      sourceLinks->size(), std::numeric_limits<std::size_t>::max());

  for (unsigned int iresult = 0; iresult < results.size(); iresult++) {
    if (not results.at(iresult).ok()) {
      continue;
    }

    auto& ckfResult = results.at(iresult).value();
    auto& measIndexes = ckfResult.lastMeasurementIndices;

    for (auto measIndex : measIndexes) {
      ckfResult.fittedStates.visitBackwards(measIndex, [&](const auto& state) {
        if (not state.typeFlags().test(Acts::TrackStateFlag::MeasurementFlag))
          return;

        std::size_t hitIndex = state.uncalibrated().index();

        // Check if hit not already used
        if (firstTrackOnTheHit.at(hitIndex) ==
            std::numeric_limits<std::size_t>::max()) {
          firstTrackOnTheHit.at(hitIndex) = iresult;
          firstStateOnTheHit.at(hitIndex) = state.index();
          return;
        }

        // if already used, control if first track state has been marked
        // as shared
        int indexFirstTrack = firstTrackOnTheHit.at(hitIndex);
        int indexFirstState = firstStateOnTheHit.at(hitIndex);
        if (not results.at(indexFirstTrack).value().fittedStates.getTrackState(indexFirstState).typeFlags().test(Acts::TrackStateFlag::SharedHitFlag))
          results.at(indexFirstTrack).value().fittedStates.getTrackState(indexFirstState).typeFlags().set(Acts::TrackStateFlag::SharedHitFlag);

        // Decorate this track
        results.at(iresult).value().fittedStates.getTrackState(state.index()).typeFlags().set(Acts::TrackStateFlag::SharedHitFlag);
      });
    }
  }
}
