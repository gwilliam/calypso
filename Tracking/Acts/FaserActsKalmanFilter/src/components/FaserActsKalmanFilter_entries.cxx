/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

#include "FaserActsKalmanFilter/FaserActsKalmanFilterAlg.h"
#include "FaserActsKalmanFilter/CombinatorialKalmanFilterAlg.h"
//#include "FaserActsKalmanFilter/MultiTrackFinderTool.h"
//#include "FaserActsKalmanFilter/TruthBasedInitialParameterTool.h"
//#include "FaserActsKalmanFilter/TruthTrackFinderTool.h"
//#include "FaserActsKalmanFilter/SPSeedBasedInitialParameterTool.h"
//#include "FaserActsKalmanFilter/SPSimpleInitialParameterTool.h"
//#include "FaserActsKalmanFilter/TrajectoryWriterTool.h"
//#include "FaserActsKalmanFilter/SimWriterTool.h"
//#include "FaserActsKalmanFilter/TruthSeededTrackFinderTool.h"
//#include "FaserActsKalmanFilter/ProtoTrackWriterTool.h"
#include "FaserActsKalmanFilter/RootTrajectoryStatesWriterTool.h"
#include "FaserActsKalmanFilter/RootTrajectorySummaryWriterTool.h"
//#include "FaserActsKalmanFilter/SegmentFitClusterTrackFinderTool.h"
//#include "FaserActsKalmanFilter/SegmentFitTrackFinderTool.h"
//#include "FaserActsKalmanFilter/ClusterTrackSeedTool.h"
#include "FaserActsKalmanFilter/ThreeStationTrackSeedTool.h"
#include "FaserActsKalmanFilter/PerformanceWriterTool.h"


DECLARE_COMPONENT(FaserActsKalmanFilterAlg)
DECLARE_COMPONENT(CombinatorialKalmanFilterAlg)
//DECLARE_COMPONENT(TruthBasedInitialParameterTool)
//DECLARE_COMPONENT(SPSeedBasedInitialParameterTool)
//DECLARE_COMPONENT(SPSimpleInitialParameterTool)
//DECLARE_COMPONENT(TrajectoryWriterTool)
//DECLARE_COMPONENT(TruthTrackFinderTool)
//DECLARE_COMPONENT(SimWriterTool)
//DECLARE_COMPONENT(TruthSeededTrackFinderTool)
//DECLARE_COMPONENT(ProtoTrackWriterTool)
//DECLARE_COMPONENT(SegmentFitClusterTrackFinderTool)
//DECLARE_COMPONENT(SegmentFitTrackFinderTool)
DECLARE_COMPONENT(RootTrajectoryStatesWriterTool)
DECLARE_COMPONENT(RootTrajectorySummaryWriterTool)
//DECLARE_COMPONENT(MultiTrackFinderTool)
//DECLARE_COMPONENT(ClusterTrackSeedTool)
DECLARE_COMPONENT(ThreeStationTrackSeedTool)
DECLARE_COMPONENT(PerformanceWriterTool)
