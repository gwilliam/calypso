# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from WaveformConditionsTools.WaveformCableMappingConfig import WaveformCableMappingCfg

def WaveByteStreamCfg(configFlags, **kwargs):
    acc = WaveformCableMappingCfg(configFlags, **kwargs)
    return acc
        
