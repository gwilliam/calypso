/*
  Copyright (C) 2021 CERN for the benefit of the FASER collaboration
*/

/**
 * @file PseudoSimToWaveformRec.cxx
 * Implementation file for the PseudoSimToWaveformRec class
 * @ author E. Torrence, 2021
 **/

#include "PseudoSimToWaveformRecTool.h"

#include "xAODFaserWaveform/WaveformHit.h"

#include "TH1F.h"
#include "TF1.h"
#include "TFitResult.h"
#include "TFitResultPtr.h"
#include "TGraph.h"

#include <vector>
#include <tuple>
#include <math.h>

// Constructor
PseudoSimToWaveformRecTool::PseudoSimToWaveformRecTool(const std::string& type, const std::string& name, const IInterface* parent) :
  base_class(type, name, parent)
{
}

// Initialization
StatusCode
PseudoSimToWaveformRecTool::initialize() {
  ATH_MSG_INFO( name() << "::initalize()" );
  return StatusCode::SUCCESS;
}


